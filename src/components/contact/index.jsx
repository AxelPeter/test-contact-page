import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

const Contact = () => {
  return (
    <div className="grid grid-cols-3 gap-6">
      <div className="col-span-1">
        <div className="px-4 sm:px-0">
          <h3 className="text-lg font-medium leading-6 text-gray-900">Contact</h3>
          <p className="mt-1 text-sm text-gray-600">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Pariatur dolor est animi ex architecto ad et adipisci officia error doloremque cupiditate nam voluptates laboriosam aliquam sunt sapiente, quod temporibus officiis.
          </p>
        </div>
      </div>
      <div className="col-span-2">
        <Formik>
          {(formik) => (
            <form noValidate>
              <div className="shadow overflow-hidden sm:rounded-md">
                <div className="px-4 py-5 bg-white sm:p-6">
                  <div className="grid grid-cols-6 gap-6">
                    <div className="col-span-3">
                      <label htmlFor="firstname" className="form-label">
                        Prénom
                      </label>
                      <input 
                        type="text" 
                        name="firstname"
                        id="firstname"
                        className="form-input"
                      />
                    </div>

                    <div className="col-span-3">
                      <label htmlFor="lastname" className="form-label">
                        Nom
                      </label>
                      <input 
                        type="text" 
                        name="lastname"
                        id="lastname"
                        className="form-input"
                      />
                    </div>

                    <div className="col-span-4">
                      <label htmlFor="email" className="form-label">
                        Email
                      </label>
                      <input 
                        type="email" 
                        name="email"
                        id="email"
                        placeholder="you@example.com"
                        className="form-input"
                      />
                    </div>

                    <div className="col-span-3">
                      <label htmlFor="type" className="form-label">
                        Type
                      </label>
                      <select
                        id="type"
                        name="type"
                        className="form-select"
                      >
                        <option value="" disabled>Sélectionner</option>
                        <option value="contact">Contact</option>
                        <option value="claim">Réclamation</option>
                      </select>
                    </div>

                    <div className="col-span-3">
                      <label htmlFor="orderNumber" className="form-label">
                        Numéro de commande
                      </label>
                      <input 
                        type="text"
                        name="orderNumber"
                        id="orderNumber"
                        className="form-input"
                      />
                    </div>

                    <div className="col-span-4">
                      <label htmlFor="subject" className="form-label">
                        Objet
                      </label>
                      <input 
                        type="text" 
                        name="subject"
                        id="subject"
                        className="form-input"
                      />
                    </div>

                    <div className="col-span-6">
                      <label htmlFor="message" className="form-label">
                        Message
                      </label>
                      <textarea
                        id="message"
                        name="message"
                        rows="5"
                        className="form-input"
                      />
                    </div>
                  </div>
                </div>
                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                  <button type="submit" className="btn">
                    Envoyer
                  </button>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Contact;
